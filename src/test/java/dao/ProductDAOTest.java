package dao;

import domain.Product;
import java.math.BigDecimal;
import java.util.Collection;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import org.hamcrest.Matchers;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.IsNull.nullValue;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ProductDAOTest {

    @BeforeAll
    public static void initialise() {
        JdbiDaoFactory.setJdbcUri("jdbc:h2:mem:tests;INIT=runscript from 'src/main/java/dao/schema.sql'");
    }

    private ProductDAO dao;

    private Product s1;
    private Product s2;
    private Product s3;

    @BeforeEach
    public void setUp() {
        dao = new ProductCollectionsDAO();
//        dao = DaoFactory.getProductDAO();

        s1 = new Product("1111", "S1_NAME", "S1_DESCRIPTION", "S1_CATEGORY", new BigDecimal(111), new BigDecimal(11));
        s2 = new Product("2222", "S2_NAME", "S2_DESCRIPTION", "S2_CATEGORY", new BigDecimal(222), new BigDecimal(22));
        s3 = new Product("3333", "S3_NAME", "S3_DESCRIPTION", "S3_CATEGORY", new BigDecimal(333), new BigDecimal(33));

        dao.saveProduct(s1);
        dao.saveProduct(s2);
        // intentionally not saving s3
    }

    @AfterEach
    public void tearDown() {
        dao.removeProduct(s1);
        dao.removeProduct(s2);
        dao.removeProduct(s3);
    }

    @Test
    public void testSaveProduct() {
        // make sure that s3 does not yet exist
        assertThat(dao.searchById(s3.getProductId()), is(nullValue()));
        assertThat(dao.getProducts(), hasSize(2));

        // save s3
        dao.saveProduct(s3);

        // make sure that s3 now exists
        assertThat(dao.searchById(s3.getProductId()), is(s3));
        assertThat(dao.getProducts(), hasSize(3));
    }

    @Test
    public void testRemoveProduct() {
        // make sure that s1 already exists
        assertThat(dao.searchById(s1.getProductId()), is(s1));
        assertThat(dao.getProducts(), hasSize(2));

        // delete s1
        dao.removeProduct(s1);

        // make sure that s1 no longer exists
        assertThat(dao.searchById(s1.getProductId()), is(nullValue()));
        assertThat(dao.getProducts(), hasSize(1));
    }

    @Test
    public void testGetProducts() {
        Collection<Product> products = dao.getProducts();

        assertThat(products, hasSize(2));
        assertThat(products, hasItem(s1));
        assertThat(products, hasItem(s2));

        // make sure that we haven't swapped/lost any fields
        Product result = products.stream()
                .filter(s -> s.getProductId().equals(s1.getProductId())).findFirst().get();
        assertThat(result, Matchers.samePropertyValuesAs(s1));
    }

    @Test
    public void testGetCategories() {
        Collection<String> categories = dao.getCategories();

        assertThat(categories, hasSize(2));
        assertThat(categories, hasItem(s1.getCategory()));
        assertThat(categories, hasItem(s2.getCategory()));

    }

    @Test
    public void testSearchByID() {
        Product result = dao.searchById(s1.getProductId());

        assertThat(result, is(s1));
        assertThat(result, Matchers.samePropertyValuesAs(s1));
    }

    @Test
    public void testFilterByCategory() {
        Collection<Product> results = dao.filterByCategory(s1.getCategory());

        assertThat(results, hasSize(1));
        assertThat(results, hasItem(s1));
    }

}
