package dao;

import domain.Customer;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import org.hamcrest.Matchers;
import static org.hamcrest.core.IsNull.nullValue;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CustomerDAOTest {

    @BeforeAll
    public static void initialise() {
        JdbiDaoFactory.setJdbcUri("jdbc:h2:mem:tests;INIT=runscript from 'src/main/java/dao/schema.sql'");
    }

    private CustomerDAO dao;

    private Customer s1;
    private Customer s2;
    private Customer s3;

    @BeforeEach
    public void setUp() {

//        dao = new CustomerCollectionsDAO();
        dao = DaoFactory.getCustomerDAO();

        s1 = new Customer("S1_USERNAME", "S1_FIRSTNAME", "S1_SURNAME", "S1_PASSWORD", "S1_ADDRESS", "S1@EMAIL");
        s2 = new Customer("S2_USERNAME", "S2_FIRSTNAME", "S2_SURNAME", "S2_PASSWORD", "S2_ADDRESS", "S2@EMAIL");
        s3 = new Customer("S3_USERNAME", "S3_FIRSTNAME", "S3_SURNAME", "S3_PASSWORD", "S3_ADDRESS", "S3@EMAIL");

        dao.saveCustomer(s1);
        dao.saveCustomer(s2);
        // intentionally not saving s3
    }

    @AfterEach
    public void tearDown() {
        dao.removeCustomer(s1);
        dao.removeCustomer(s2);
        dao.removeCustomer(s3);
    }

    @Test
    public void testSaveCustomer() {
        // make sure that s3 does not yet exist
        assertThat(dao.searchByUsername(s3.getUsername()), is(nullValue()));

        // save s3
        dao.saveCustomer(s3);

        // make sure that s3 now exists
        assertThat(dao.searchByUsername(s3.getUsername()), is(s3));
        assertThat(dao.currentCustomer(s3.getUsername(), s3.getPassword()), is(true));
    }

    @Test
    public void testRemoveCustomer() {
        // make sure that s1 already exists
        assertThat(dao.searchByUsername(s1.getUsername()), is(s1));
        assertThat(dao.currentCustomer(s1.getUsername(), s1.getPassword()), is(true));

        // delete s1
        dao.removeCustomer(s1);

        // make sure that s1 no longer exists
        assertThat(dao.searchByUsername(s1.getUsername()), is(nullValue()));
    }

    @Test
    public void testCurrentCustomer() {
        assertThat(dao.searchByUsername(s1.getUsername()), is(s1));

        boolean s1CurrentUser = dao.currentCustomer(s1.getUsername(), s1.getPassword());

        assertThat(s1CurrentUser, is(true));

    }

    @Test
    public void testSearchByUsername() {
        Customer result = dao.searchByUsername(s1.getUsername());

        assertThat(result, is(s1));
        assertThat(result, Matchers.samePropertyValuesAs(s1, "customerId"));
    }

}
