package web;

import dao.CustomerDAO;
import domain.Customer;
import io.jooby.Jooby;
import io.jooby.StatusCode;
import net.sf.oval.Validator;

/**
 *
 * @author taraglennie
 */
public class CustomerModule extends Jooby {

    public CustomerModule(CustomerDAO dao) {

        get("/api/customer/{username}", ctx -> {

            String username = ctx.path("username").value();

            Customer customer = dao.searchByUsername(username);

            if (customer == null) {
                // no student with that ID found, so return a 404/Not Found error
                return ctx.send(StatusCode.NOT_FOUND);
            } else {
                return customer;
            }
        });

        post("/api/customers", ctx -> {
            Customer customer = ctx.body().to(Customer.class);
            new Validator().assertValid(customer);
            dao.saveCustomer(customer);
            return ctx.send(StatusCode.CREATED);

        });

    }

}
