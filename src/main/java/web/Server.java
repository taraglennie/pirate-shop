package web;

import dao.CustomerCollectionsDAO;
import dao.CustomerDAO;
import dao.DaoFactory;
import dao.ProductCollectionsDAO;
import dao.ProductDAO;
import dao.SaleDAO;
import domain.Customer;
import domain.Product;
import io.jooby.Jooby;
import io.jooby.ServerOptions;
import io.jooby.json.GsonModule;
import java.math.BigDecimal;
import java.util.Set;

public class Server extends Jooby {

    ProductDAO dao = DaoFactory.getProductDAO();
    CustomerDAO customerDao = DaoFactory.getCustomerDAO();
    SaleDAO saleDao = DaoFactory.getSaleDAO();

    public Server() {
        setServerOptions(new ServerOptions().setPort(8085));

        mount(new StaticAssetModule());

        install(new GsonModule());

        install(new BasicAccessAuth(customerDao, Set.of("/api/.*"), Set.of("/api/customers")));

        mount(new ProductModule(dao));

        mount(new CustomerModule(customerDao));

        mount(new SaleModule(saleDao));

    }

    public static void main(String[] args) {

        ProductDAO dao = new ProductCollectionsDAO();
        dao.saveProduct(new Product("1", "Pirate Hat", "Stylish", "Hat", new BigDecimal("5"), new BigDecimal("13")));
        dao.saveProduct(new Product("2", "Baseball Hat", "Not Stylish", "Hat", new BigDecimal("1"), new BigDecimal("13")));
        dao.saveProduct(new Product("3", "Silver Hook", "Swanky", "Hook", new BigDecimal("20"), new BigDecimal("10")));

        CustomerDAO custyDao = new CustomerCollectionsDAO();
        custyDao.saveCustomer(new Customer("nmo", "pete", "grey", "password", "address", "pete@gmail.com"));

        System.out.println("\nStarting Server.");
        new Server().start();
    }

}
