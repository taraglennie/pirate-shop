package domain;

import java.util.Objects;
import net.sf.oval.constraint.Length;
import net.sf.oval.constraint.NotBlank;
import net.sf.oval.constraint.NotNull;

/**
 * @author Mark George
 */
public class Customer {

    private Integer customerId;
    @NotNull(message = "Username must be provided.")
    @NotBlank(message = "Username must be provided.")
    @Length(min = 2, message = "Username must contain at least two characters.")
    private String username;
    
    @NotNull(message = "First name must be provided.")
    @NotBlank(message = "First name must be provided.")
    private String firstName;
    
    @NotNull(message = "Last name must be provided.")
    @NotBlank(message = "Last name must be provided.")
    private String surname;
    
    @NotNull(message = "Password must be provided.")
    @NotBlank(message = "Password must be provided.")
    @Length(min = 5, message = "Password must contain at least five characters.")
    private String password;
    
    @NotNull(message = "Email must be provided.")
    @NotBlank(message = "Email must be provided.")
    @Length(min = 5, message = "Email must contain at least five characters.")
    private String email;
    
    @NotNull(message = "Address must be provided.")
    @NotBlank(message = "Address must be provided.")
    @Length(min = 10, message = "Address must contain at least ten characters.")
    private String address;

    public Customer() {
    }

    public Customer(String username, String firstName, String surname, String password, String shippingAddress, String emailAddress) {
        this.username = username;
        this.firstName = firstName;
        this.surname = surname;
        this.password = password;
        this.address = shippingAddress;
        this.email = emailAddress;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer personId) {
        this.customerId = personId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.username);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Customer other = (Customer) obj;
        return Objects.equals(this.username, other.username);
    }
    
    

    @Override
    public String toString() {
        return "Customer{" +"username=" + username + ", firstName=" + firstName + ", surname=" + surname + ", password=" + password + ", email=" + email + ", address=" + address + '}';
    }

}
