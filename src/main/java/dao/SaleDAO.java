package dao;

import domain.Sale;

/**
 *
 * @author taraglennie
 */
public interface SaleDAO {

    void save(Sale sale);

}
