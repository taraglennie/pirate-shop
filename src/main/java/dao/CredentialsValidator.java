package dao;

/**
 *
 * @author taraglennie
 */
public interface CredentialsValidator {
     Boolean currentCustomer(String username, String password);
}
