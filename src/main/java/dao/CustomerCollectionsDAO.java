package dao;

import domain.Customer;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author taraglennie
 */
public class CustomerCollectionsDAO implements CustomerDAO {

    private static final Map<String, Customer> customers = new HashMap<>();

    @Override
    public void saveCustomer(Customer customer){
        customers.put(customer.getUsername(), customer);
    }
    
    @Override
    public void removeCustomer(Customer customer){
        customers.remove(customer.getUsername(), customer);
    }
    
    
    @Override
    public Customer searchByUsername(String username) {
        return customers.get(username);
    }
    
    @Override
    public Boolean currentCustomer(String username, String password) {
        return password.equals(customers.get(username).getPassword());
    }
}

    

   

