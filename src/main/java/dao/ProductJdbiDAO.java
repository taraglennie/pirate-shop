package dao;

import domain.Product;
import java.util.Collection;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

/**
 *
 * @author taraglennie
 */
public interface ProductJdbiDAO extends ProductDAO {

    @Override
    @SqlQuery ("SELECT * FROM PRODUCTS WHERE PRODUCT_ID = :productId")
    @RegisterBeanMapper(Product.class)
    public Product searchById(@Bind("productId")String id);

    @Override
    @SqlUpdate("INSERT INTO PRODUCTS(PRODUCT_ID, NAME, DESCRIPTION, CATEGORY, LISTPRICE, QUANTITY_IN_STOCK) values (:productId, :name, :description, :category, :listPrice, :quantityInStock)")
    public void saveProduct(@BindBean Product product);

    @Override
    @SqlUpdate("DELETE FROM PRODUCTS WHERE PRODUCT_ID = :productId")
    public void removeProduct(@BindBean Product product);

    @Override
    @SqlQuery("SELECT * FROM PRODUCTS")
    @RegisterBeanMapper(Product.class)
    public Collection<Product> getProducts();

    @Override
    @SqlQuery ("SELECT CATEGORY FROM PRODUCTS GROUP BY CATEGORY")
    @RegisterBeanMapper(Product.class)
    public Collection<String> getCategories();

    @Override
    @SqlQuery("SELECT * FROM PRODUCTS WHERE CATEGORY = :category")
    @RegisterBeanMapper(Product.class)
    public Collection<Product> filterByCategory(@Bind("category")String category);
    
}
