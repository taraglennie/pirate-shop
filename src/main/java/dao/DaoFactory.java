package dao;

/**
 *
 * @author taraglennie
 */
public class DaoFactory {

    public static CustomerDAO getCustomerDAO() {
        //return JdbiDaoFactory.getCustomerDAO();
        return new CustomerCollectionsDAO();
    }

    public static ProductDAO getProductDAO() {
        //return JdbiDaoFactory.getProductDAO();
        return new ProductCollectionsDAO();

    }

    public static SaleDAO getSaleDAO() {
        return JdbiDaoFactory.getSaleDAO();
        //return new ProductCollectionsDAO();

    }
}
