package dao;

import domain.Customer;

/**
 *
 * @author taraglennie
 */
public interface CustomerDAO extends CredentialsValidator{
    

	void removeCustomer(Customer customer);

	void saveCustomer(Customer customer);

	Customer searchByUsername(String username);
        
    Boolean currentCustomer(String username, String password);

}
    

