package dao;

import domain.Customer;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

/**
 *
 * @author taraglennie
 */
public interface CustomerJdbiDAO extends CustomerDAO {

    @Override
    @SqlQuery("SELECT EXISTS(SELECT * FROM CUSTOMERS WHERE USERNAME = :username AND PASSWORD = :password)")
    public Boolean currentCustomer(@Bind("username") String username, @Bind("password") String password);

    @Override
    @SqlQuery("SELECT * FROM CUSTOMERS WHERE USERNAME = :username")
    @RegisterBeanMapper(Customer.class)
    public Customer searchByUsername(@Bind("username") String username);

    @Override
    @SqlUpdate("INSERT INTO CUSTOMERS(USERNAME, FIRST_NAME, SURNAME, PASSWORD, ADDRESS, EMAIL) values (:username, :firstName, :surname, :password, :address, :email)")
    public void saveCustomer(@BindBean Customer customer);

    @Override
    @SqlUpdate("DELETE FROM CUSTOMERS WHERE USERNAME = :username")
    public void removeCustomer(@BindBean Customer customer);

}
