# All Things Pirate
### Made By Tara Glennie

All things pirate is an online shopping system, which was built for a project at univeristy. It uses Jdbi as its persistent data storage, Jooby for the back end web services and Vue.js for its front end web interfaces. Note this is a fictional shop and buying something does not mean you will recieve the product.

It has functionalities including:
- Sign in
- Sign out 
- Browsing Products
- Filtering Products
- Adding Products to Cart

## Installation and Running of Program
Prerequisites: JDK will need to be installed prior, also assumes that the Gradle extension is installed on VS Code

Note: Some of the tests use Mockito, which requires the abiltiy to control your computer. (The permission to allow this for mac can be found under System Settings -> Privacy and Security -> Accessibility -> Then turn on for Visual Studio Code). If you would rather not allow this, then once you've opened the project in your IDE (instructions below) simply go into the "build.gradle" file in the root of the project, under "test", uncomment "onlyIf { project.gradle.startParameter.taskNames.contains("test") }". 


- Download the repository
#### Starting the Database
This system can use an H2 database with the schema as described under src -> main -> dao -> schema.sql.  However it is by default using a collection based storage system which is not persistent. For this, there is no set up. (Also please note if you're using the collections based system you won't be able to "Check Out" your Cart)

To Set up an H2 database:
- Install H2 from http://www.h2database.com/html/main.html
- Run H2
- Create a new database by right clicking (windows) or clicking (mac) on the H2 symbol in the task bar, then clicking "create new database.."
- Enter the database path as "tcp://localhost/~/[Wherever you want to save the database]"
- Enter the username and password as "sa"
- Click create
- Copy the JDBC URL
- Go onto the H2 console by clicking the H2 symbol in the taskbar
- Using these settings enter your newly made database:
  - Saved Settings "Generic H2 (Server)" 
  - Driver Class "org.h2.Driver"
  - JDBC URL: the URL you copied earlier
  - User Name: "sa"
  - Password: "sa"
- Copy the all four table schemas from src -> main -> dao -> schema.sql
- Paste and run those schemas into the main textbox, on the main screen
- Copy the JDBC URL and replace (in src/main/java/dao/JdbiDaoFactory.java) "private static String jdbcUri = "jdbc:h2:tcp://localhost/pirates"; current link with your JDBC url.

#### Running the App
- Open your preferred IDE to the project folder "pirate-shop" (Instructions will be for VS Code)
- Click on the Gradle tab in the side bar
- Click on the "INFO202 Milestone 2" drop down
- Click on the "Build" drop down
- Right click on "Build" and click "Run Task"
- Click on the "Tasks" drop down
- Click on the "Application" drop down
- Right click on "Run" and click "Run Task"
- Use the link in the terminal "http://localhost:8085/" 
- The website should now be viewable

## How to Use the Application
#### Using the WebApp
From the homepage you will have the option to "Sign In", to use this Web App you must create an account. 
From the sign in page, you can either sign in with existing credentials, or sign up using the link. 

Once you are signed in, you will have the abiltiy to view products, filter products, add products to your cart, and "buy products." 
Please note this is a completely fake site and buying a product does not mean you will recieve the product. 

#### Adding Products to the Database (if you are using the H2 Database)
(This assumes you have the database running, look above for information)

On VS Code in the sidebar from the root project, go to src -> main -> java -> ProductCatalogue.java. Right click on it in the file explorer and click "Run Java."

This will bring up a GUI for you to "Add a New Product" or "View Products" Click on "Add New Product" and enter the information for all text boxes. 

#### Removing Products from the Database (if you are using the H2 Database)
(This assumes you have the database running, look above for information)

On VS Code in the sidebar from the root project, go to src -> main -> java -> ProductCatalogue.java. Right click on it in the file explorer and click "Run Java."

This will bring up a GUI for you to "Add a New Product" or "View Products" Click on "View Products" and select in the list the product you wish to delete. Then press the delete button.


## How to Run the JUnit Testing 
To run/view the JUnit tests the base java project folder "pirate-shop" needs to be opened in an IDE, e.g. VSCode. Note the build system will also automatically run these tests.
- Click on the gradle tab on the left sidebar (the elephant)
- Click on the "App" drop down
- Click on the "Tasks" drop down 
- Click on the "Verification" drop down 
- Right click on "Test" and click "Run Task"




