var salesApi = '/api/sales';
"use strict";

class SaleItem {
    constructor(product, quantityPurchased) {
        this.product = product;
        this.quantityPurchased = quantityPurchased;
        this.salePrice = product.listPrice;
    }
}

class Sale {
    constructor(customer, items) {
        this.customer = customer;
        this.items = items;
    }
}

const app = Vue.createApp({

    data() {
        return {

        };
    },

    computed: Vuex.mapState({
        product: 'selectedProduct',
        items: 'items',
        customer: 'customer'
    }),

    methods: {
        // comma separated function declarations
        addToCart() {
            if (this.product.quantityInStock < this.quantity) {
                alert("Note ordering more than the quantity in stock means items may take longer to ship as we get more for the supplier")
            }
            ;
            dataStore.commit("addItem", new SaleItem(this.product, this.quantity));
            window.location = "view-products.html"
        },

        checkOut() {
            if (this.items.length === 0) {
                alert("No items to check out")
            } else {
                let sale = new Sale(this.customer, this.items);
                axios.post(salesApi, sale)
                        .then(() => {
                            dataStore.commit("clearItems");
                            window.location = 'order-conformation.html';
                        })
                        .catch(error => {
                            alert(error.response.data.message);
                        });
            }
        },

        getItemTotal(item) {
            return item.quantityPurchased * item.product.listPrice;
        },

        grandTotal() {
            let total = 0;
            for (let i = 0; i < this.items.length; i++) {
                total = total + (this.items[i].quantityPurchased * this.items[i].product.listPrice);
            }
            return total;
        }

    },

    mixins: [NumberFormatter, BasicAccessAuthentication]

});

/* other component imports go here */
import { navigationMenu } from './navigation-menu.js';

// register the navigation menu under the <navmenu> tag
app.component('navmenu', navigationMenu);

// import data store
import { dataStore } from './data-store.js'
        app.use(dataStore);

import { NumberFormatter } from './number-formatter.js';

// import authentication module
import { BasicAccessAuthentication } from './authentication.js';



// mount the page
app.mount("main");