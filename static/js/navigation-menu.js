"use strict";

export const navigationMenu = {

    computed: {
        signedIn() {
            return this.customer != null;
        },
        ...Vuex.mapState({
                customer: 'customer'
        })
    },

    template:
            `
    <nav>
    <div>
        <a href="index.html"><button>Home</button></a>
        <a href="view-products.html" v-if="signedIn"><button>Browse Products</button></a>
        <a href="check-out.html" v-if="signedIn"><button>View Cart</button></a>
        <a href="index.html" v-if="signedIn" @click="signOut()"><button>Sign Out</button></a>
        <a href="sign-in.html" v-if="!signedIn"><button>Sign In</button></a>
    <div class="center" id="text" v-if="signedIn">Welcome {{customer.firstName}}</div>
    </div>
    </nav>
    `,

    methods: {
        signOut() {
            sessionStorage.clear();
            window.location = 'index.html';
        }
    }
};



