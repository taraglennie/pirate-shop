var customersFilterApi = ({username}) => `/api/customer/${username}`;

const app = Vue.createApp({

    data() {
        return {
            customer: new Object()

        };
    },

    methods: {
        signIn(username) {
            this.createToken(this.customer.username, this.customer.password);
            axios.get(customersFilterApi({'username': username}))
                    .then(response => {
                        this.customer = response.data;
                        dataStore.commit("signIn", this.customer);
                        window.location = 'index.html';
                    })
                    .catch(error => {
                        console.error(error);
                        alert("An error occurred with username or password. Please try again.");
                        //alert(error.response.data.message);
                    });
        }


    },

    // other modules
    mixins: [BasicAccessAuthentication]

});

// import the navigation menu
import { navigationMenu } from './navigation-menu.js';

// register the navigation menu under the <navmenu> tag
app.component('navmenu', navigationMenu);

// import data store
import { dataStore } from './data-store.js'
        app.use(dataStore);


// import authentication module
import { BasicAccessAuthentication } from './authentication.js';

// mount the page
app.mount("main");

